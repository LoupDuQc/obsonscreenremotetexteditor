// Create WebSocket connection.
const socket = new WebSocket('ws://localhost:8080');

// Connection opened
socket.addEventListener('open', function (event) {
	socket.send(JSON.stringify({ register: "controller" }));
	let textInput = document.getElementById("text");
	textInput.addEventListener("input", e => {
		console.log({ text: e.target.value });
		socket.send(JSON.stringify({ text: e.target.value }));
	});
	let colorInput = document.getElementById("color");
	colorInput.addEventListener("input", e=>{
		console.log({ color: e.target.value });
		socket.send(JSON.stringify({ color: e.target.value }));
	});
	let sizeInput = document.getElementById("size");
	let sizeSliderInput = document.getElementById("sizeSlider");
	sizeInput.addEventListener("input", e=>{
		console.log({ size: e.target.value });
		sizeSliderInput.value = e.target.value;
		if(e.target.value != ""){// Because decimals are a pain
			socket.send(JSON.stringify({ size: e.target.value }));
		}
	});
	sizeSliderInput.addEventListener("input", e=>{
		console.log({ size: e.target.value });
		sizeInput.value = e.target.value;
		if(e.target.value != ""){// Because decimals are a pain
			socket.send(JSON.stringify({ size: e.target.value }));
		}
	});
	let alignInput = document.getElementById("textAlign");
	alignInput.addEventListener("input", e=>{
		console.log({ position: e.target.value });
		socket.send(JSON.stringify({ position: e.target.value }));
	});
});

// Listen for messages
socket.addEventListener('message', function (event) {
	console.log('Message from server ', event.data);
	let textInput = document.getElementById("text");
	let parsedMessage = JSON.parse(event.data);
	textInput.value = parsedMessage.text;
	let colorInput = document.getElementById("color");
	colorInput.value = parsedMessage.color;
	let sizeInput = document.getElementById("size");
	sizeInput.value = parsedMessage.size;
	let sizeSliderInput = document.getElementById("sizeSlider");
	sizeSliderInput.value = parsedMessage.size;
	let alignInput = document.getElementById("textAlign");
	alignInput.value = parsedMessage.position;
});

