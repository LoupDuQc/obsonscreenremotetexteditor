var express = require('express');
const WebSocket = require('ws');
const fs = require("fs/promises");
var app = express();
const morgan = require('morgan');// Loggers
app.use(morgan(':method :url :status :res[content-length] - :response-time ms'));

app.use('/obs', express.static('./obsPage'));
app.use('/controller', express.static('./clientPage'));

app.listen(3000);

let obsClientList = new Set();
let controllerClientList = new Set();

const wss = new WebSocket.Server({ port: 8080 });

let data = {
	text: "",
	color: "#000000",// Black
	size: 12,
	position:"left"
}

try{
	data = require("./data");
} catch{
	console.error("Could not load the previous settings, ignore this error if you're running it for the first time or you deleted said settings on purpose")
}

function saveData(){
	fs.writeFile("./data.json", JSON.stringify(data));
}

function updateOBSClients(){
	obsClientList.forEach(obsWS => {
		obsWS.send(JSON.stringify(data));
	});
}

wss.on('connection', function connection(ws, req) {
	console.log("Connection")
	ws.on('message', function incoming(message) {
		console.log('received: %s', message);
		let parsedMessage = JSON.parse(message);
		if (parsedMessage.register != undefined) {
			switch (parsedMessage.register) {
				case "obs": obsClientList.add(ws); ws.send(JSON.stringify(data)); break;
				case "controller": controllerClientList.add(ws); ws.send(JSON.stringify(data)); break;
			}
		} else if(parsedMessage.text != undefined){
			data.text = parsedMessage.text;
			saveData();
			updateOBSClients();
		} else if (parsedMessage.size != undefined){
			data.size = parsedMessage.size;
			saveData();
			updateOBSClients();
		} else if (parsedMessage.position != undefined){
			data.position = parsedMessage.position;
			saveData();
			updateOBSClients();
		} else if (parsedMessage.color != undefined){
			data.color = parsedMessage.color;
			saveData();
			updateOBSClients();
		} else if (parsedMessage.font != undefined){
			//do something
		} 
	});
	
	ws.on('close', function close() {
		obsClientList.delete(ws);
		controllerClientList.delete(ws);
	});
});

wss.on('close', function close() {
	console.log("test")
});