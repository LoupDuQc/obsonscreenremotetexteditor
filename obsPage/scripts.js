// Create WebSocket connection.
const socket = new WebSocket('ws://localhost:8080');

// Connection opened
socket.addEventListener('open', function (event) {
	socket.send(JSON.stringify({ register: "obs" }));
});

// Listen for messages
socket.addEventListener('message', function (event) {
	console.log('Message from server ', event.data);
	let container = document.getElementById("content");
	console.log('received: %s', event.data);
	let parsedMessage = JSON.parse(event.data);
	container.innerText = parsedMessage.text;
	container.style.color = parsedMessage.color;
	container.style.fontSize = parsedMessage.size + "px";
	container.style.textAlign = parsedMessage.position;
});